const Order = require("../models/orderModel");
const Product = require('../models/productModel');
const User = require('../models/userModel');

module.exports.buyNow = async (userId, newOrder) => {
  try {
      if (newOrder.products === undefined || newOrder.products.length === 0) {
          return { error: 'Check out must not be empty' };
      }
      let totalAmount = 0;
      for (let i = 0; i < newOrder.products.length; i++) {
          const product = newOrder.products[i];
          const { price } = await Product.findById(product.productId);
          totalAmount += price * product.quantity;
      }
      const order = new Order({
          userId,
          products: newOrder.products,
          totalAmount,
          address: newOrder.address,
          status: "Success",
          purchaseOn: Date.now()
      });
      await order.save();
      return { message: 'Order placed successfully', order };
  } catch (error) {
      return { error: error.message };
  }
};
