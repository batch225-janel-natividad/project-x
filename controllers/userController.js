const User = require("../models/userModel");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const jwt = require("jsonwebtoken");


module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        email : reqBody.email,
        password : bcrypt.hashSync(reqBody.password, 10)
        })

    return newUser.save().then((user, error) => {
        if (error) {
            return false;
        } else {
            return user
        };
    });
};


module.exports.loginUser = (data) => {

	return User.findOne({email : data.email}).then(result => {

		if(result == null ){

			return {
				message: "Not found in our database"
			}

		} else {

			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(data.password, result.password);


			if (isPasswordCorrect) {

				return {access : auth.createAccessToken(result)}

			} else {

				// if password doesn't match
				return {
					message: "password was incorrect"
				}
			};

		};

	});
};



//Retrieve user details
module.exports.retrieveUserById = async function (data) {
    // if (!data.isAdmin) {
    //     let message = Promise.resolve('User must be Admin to Access this.')

    //     return message.then((value) => {
    //         return value
    //     })
    // }

    try {
        let result = await User.findOne( {_id: data.id} )
        if (result == null) {
            return { message: "Invalid ID" }
        } else {
            return result
        }
    } catch {
        return { message: "Error!" }
    }

}