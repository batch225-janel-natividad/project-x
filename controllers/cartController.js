const User = require("../models/userModel");
const Cart = require("../models/cartModel");

module.exports.addToCart = async (reqBody) => {
    try {
      const { userId, productId, quantity } = reqBody;
      const user = await User.findOne({ userId }).populate('cart.product.productId');
      console.log('user:', user);

      if (!user) {
        return { error: 'User not found' };
      }
  
      // Find the product being added to the cart
      const product = user.cart.product.find(p => p.productId._id.toString() === productId);
      console.log('product:', product);
      
      if (product) {
        // If the product is already in the cart, update the quantity
        product.quantity += quantity;
      } else {
        // Otherwise, add the product to the cart
        const newProduct = { productId, quantity };
        user.cart.product.push(newProduct);
      }
  
      await user.save();
  
      return { message: 'Product added to cart successfully' };
    } catch (error) {
      return { error: error.message };
    }
};
