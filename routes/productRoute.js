const router = require("express").Router();
const Product = require("../models/productModel");
const auth = require("../auth");

const productController = require("../controllers/productController");

//Create New Product
router.post("/createProduct", auth.verify, (req,res) => {
    const data = {
      product: req.body,
      isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
        productController.createProd(data).then(result => res.send(result));
    });
 
//Retrive All Products
router.get("/retriveAllProduct", auth.verify, (req,res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
        productController.retriveAllProd(data).then(result => res.send(result));
    });

//Retrive All Active
router.get("/retrievAllActive",  (req, res) => {
        productController.retrieveAllActive().then(result => res.send(result));
    });
    
//Retrive Single Product
router.get("/:productId", async (req, res) => {
    try {
    const {productId} = req.params;
    const product = await Product.findOne({_id: productId, isActive: true});
    if (!product) return res.status(404).send({ message: 'Product not found' });
    res.send(product);
    } catch (error) {
    res.status(500).send({ message: error });
    }
    });

//Updating Products
router.put("/update/:productId", auth.verify, (req, res) =>{
    const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin) {
        productController.updateProduct(req.params, req.body).then(result => res.send(result));
    } else {
        res.status(401).send({ message: "You are not authorized to update products"});
    }
});

//Archive Products
router.put("/archive/:productId", auth.verify, (req, res) =>{
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin) {
        productController.archiveProduct(req.params).then(result => res.send(result));
    } else {
        res.status(401).send({ message: "You are not authorized to archive products"});
    }
});

//Activate product
router.put("/activate/:productId", auth.verify, (req, res) =>{
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin) {
        productController.activateProduct(req.params).then(result => res.send(result));
    } else {
        res.status(401).send({ message: "You are not authorized to activate the  product"});
    }
});
module.exports = router;