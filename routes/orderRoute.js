const router = require("express").Router();
const auth = require("../auth");

const orderController = require("../controllers/orderController");
router.post("/buy-now", auth.verify, (req, res) => {
  const userId = auth.decode(req.headers.authorization).id;
  orderController.buyNow(userId, req.body)
  .then((result) => {
      res.status(200).json(result);
  })
  .catch((error) => {
      res.status(400).json(error);
  });
});

module.exports = router;
