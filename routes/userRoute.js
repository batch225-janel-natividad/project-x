const router = require("express").Router();
const auth = require("../auth");
const userController = require("../controllers/userController");
const express = require('express');

//Route for Registration
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(result => res.send(result))
})


//Route for user.authetication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
})

//Retrive user detail
router.get("/userDetails", auth.verify, async (req, res) => {

    const data = auth.decode(req.headers.authorization)

    let result = await userController.retrieveUserById(data);
    res.send(result);
})

module.exports = router;